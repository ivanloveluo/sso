/**
 * Created by 11900 on 2020/4/28.
 */
function load(type,url,data,funCallBack) {
    $.ajax({
        type: type,
        url: url,
        data:data,
        success: function (res) {
            //触发外部事件
            if (funCallBack != null && typeof funCallBack == "function"){
                funCallBack(res);
            }
        },
        'beforeSend':setHeader
    });
}

function setHeader(xhr) {
    xhr.setRequestHeader("Authorization",window.localStorage.token)
}