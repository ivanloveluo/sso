package com.ivan.exception;

import com.ivan.utils.ResultUtil;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ima.tran_common.exections
 * @Description: TODO
 * @date 2020/3/22 11:50
 */
@ControllerAdvice
public class ExceptionsHandler {
    @ResponseBody
    @ExceptionHandler(VhrException.class)//可以直接写@ExceptionHandler,不指明异常类，会自动映射
    public ResultUtil customGenericExceptionHnadler(VhrException e){ //还可以声明接收其他任意参数
        return ResultUtil.build(e.getCommonCode());
    }
}
