package com.ivan.exception;

import com.ivan.utils.CommonCode;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.exception
 * @Description: TODO
 * @date 2020/3/22 11:50
 */
public class VhrException   extends RuntimeException  {

    public VhrException(CommonCode commonCode) {
        this.commonCode = commonCode;
    }

    private CommonCode commonCode;

    public CommonCode getCommonCode() {
        return commonCode;
    }
}
