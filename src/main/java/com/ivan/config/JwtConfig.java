package com.ivan.config;

import com.ivan.utils.JwtUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.config
 * @Description: TODO
 * @date 2020/4/28 17:20
 */
@ConfigurationProperties(prefix = "jwt")
@Configuration
public class JwtConfig {
    private  int expiretime;
    private  String key; //服务器的key /** 秘钥 */
    private  String heard;
    private String iss;//签发者

    public int getExpiretime() {
        return expiretime;
    }

    public void setExpiretime(int expiretime) {
        this.expiretime = expiretime;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        JwtUtils.setJwtSecert(key);
        this.key = key;
    }

    public String getHeard() {
        return heard;
    }

    public void setHeard(String heard) {
        this.heard = heard;
    }

    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }
}
