package com.ivan.serverice;

import com.ivan.entity.User;
import com.ivan.utils.ResultUtil;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package serverice
 * @Description: TODO
 * @date 2020/4/27 11:43
 */
public interface IJwtService {
    /**
     * Description:登录获取token
     *
     * @param user user
     * @return ResultUtil
     * @date 2019/3/4 18:45
     */
    ResultUtil login(User user);

    /**
     * Description:检查jwt有效性
     *
     * @return fanxb
     * @author
     * @date 2019/3/4 18:47
     */
    ResultUtil checkJwt(String token) ;

    /**
     * 刷新
     * @param token
     * @return fanxb
     */
    ResultUtil refreshJwt(String token) ;

    /**
     * Description: 使该jwt失效
     *
     * @author fanxb
     * @date 2019/3/4 19:58
     */
      void inValid(String jwt);
}
