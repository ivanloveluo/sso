package com.ivan.utils;

import lombok.ToString;

@ToString
public enum CommonCode {

    SUCCESS(true,10000,"操作成功！"),
    FAIL(false,11111,"操作失败！"),
    DATAERR(false,20000,"数据错误！"),
    UNAUTHENTICATED(false,10001,"此操作需要登陆系统！"),
    USERNOT(false,10002,"用户不存在！"),
    USERNOTLOGIN(false,10006,"用户不存在！"),
    PWDNOT(false,10003,"密码不正确！"),
    TOKENNOT(false,10007,"token为空！"),
    VALNOT(false,10005,"用户验证失败！"),
    UNAUTHORISE(false,10004,"权限不足，无权操作！"),
    SERVER_ERROR(false,99999,"抱歉，系统繁忙，请稍后重试！");

//    private static ImmutableMap<Integer, CommonCode> codes ;
    //操作是否成功
    private   boolean status;
    //操作代码
    private int code;
    //提示信息
    private String msg;
    private CommonCode(boolean status, int code, String msg){
        this.status = status;
        this.code = code;
        this.msg = msg;
    }
    private CommonCode(int code, String msg){
        this.code = code;
        this.msg = msg;
    }



    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
