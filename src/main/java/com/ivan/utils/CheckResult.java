package com.ivan.utils;

import io.jsonwebtoken.Claims;
import lombok.Data;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.utils
 * @Description: TODO
 * @date 2020/4/27 21:53
 */
@Data
public class CheckResult {

    /**
     * 错误编码
     */
    private int errCode;
    /**
     * 是否成功
     */
    private boolean success;
    /**
     * 验证过程中payload中的数据
     */
    private Claims claims;
}
