package com.ivan.entity;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.entity
 * @Description: TODO
 * @date 2020/4/27 10:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String name;
    @JSONField(serialize = false)
    private String password;
}
