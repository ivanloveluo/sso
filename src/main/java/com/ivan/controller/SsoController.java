package com.ivan.controller;

import cn.hutool.core.util.StrUtil;
import com.ivan.config.JwtConfig;
import com.ivan.entity.User;
import com.ivan.serverice.IJwtService;
import com.ivan.utils.CommonCode;
import com.ivan.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 11900
 * @version V1.0
 * @Title: ${FILE_NAME}
 * @Package com.ivan.controller
 * @Description: TODO
 * @date 2020/4/27 11:40
 */
@RestController
public class SsoController {

    @Autowired
    private JwtConfig jwtConfig;

    @Autowired
   private IJwtService service;

    @PostMapping("/login")
    public ResultUtil login(@RequestBody User user) {
        return service.login(user);
    }

    @GetMapping("/checkJwt")
    public ResultUtil checkJwt(HttpServletRequest request) {
        String token = request.getHeader(jwtConfig.getHeard());
        if(StrUtil.isNotEmpty(token)){
            return service.checkJwt(token);
        }
        return ResultUtil.build(CommonCode.USERNOTLOGIN);
    }

    @GetMapping("/refreshJwt")
    public ResultUtil refreshJwt(HttpServletRequest request) {
        String token = jwtConfig.getHeard();
        if(StrUtil.isNotEmpty(token)){
            return service.checkJwt(token);
        }
        return ResultUtil.build(CommonCode.USERNOTLOGIN);
    }

    @GetMapping("/inValid")
    public ResultUtil inValid(HttpServletRequest request) {
        String token = request.getHeader(jwtConfig.getHeard());
        service.inValid(token);
        return ResultUtil.ok(null);
    }
}
